Subtitle Merger
Takes two subtitle files, merges text lines from the second one into the first one. So the output file has 2-4 text lines per subtitle.
Example:

Subtitle 1 from file 1:

1

00:00:00,00 --> 00:01:00,00

lorem ipsum

ipsum lorem

Subtitle 1 from file 2:

1

00:00:01,00 --> 00:02:00,00

example text here

even more example text

Subtitle 1 from output file:

1

00:00:00,00 --> 00:01:00,00

lorem ipsum

example text here

ipsum lorem

even more example text

Saves output file to filename_MERGED.srt

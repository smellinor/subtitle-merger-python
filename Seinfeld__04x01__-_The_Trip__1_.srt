1
00:00:02,176 --> 00:00:03,837
It's amazing to me
that people move...

2
00:00:05,413 --> 00:00:07,643
...thousands of miles away
to another city.

3
00:00:07,815 --> 00:00:10,079
They think nothing of it.
They get on a plane.

4
00:00:10,251 --> 00:00:12,412
They live there now.

5
00:00:12,587 --> 00:00:14,578
Just a thousand miles...
I'm living over there.

6
00:00:14,756 --> 00:00:17,691
You know, pioneers... It took years
to cross the country.

7
00:00:17,859 --> 00:00:21,852
Now people will move thousands
of miles just for one season.

8
00:00:22,030 --> 00:00:24,055
I don't think any pioneers did that.

9
00:00:24,232 --> 00:00:26,530
"Yeah, it took us a decade
to get there...

10
00:00:26,701 --> 00:00:28,328
...and we stayed for the summer.

11
00:00:28,503 --> 00:00:31,233
It was nice. It had a pool.
The kids loved it.

12
00:00:31,406 --> 00:00:34,898
And then we left about 10 years ago,
and we just got back.

13
00:00:35,076 --> 00:00:37,067
We had a great summer.
It took us 20 years...

14
00:00:37,245 --> 00:00:39,236
...and now our lives are over. "

15
00:00:44,519 --> 00:00:46,817
- Kramer was on Murphy Brown?
- Yeah.

16
00:00:46,988 --> 00:00:48,455
- Are you sure?
- Yeah.

17
00:00:49,390 --> 00:00:51,290
Murphy Brown, the TV show?

18
00:00:51,459 --> 00:00:53,552
Come on, will you?

19
00:00:53,728 --> 00:00:58,722
Kramer was on Murphy Brown?
That son of a gun.

20
00:00:58,900 --> 00:01:01,198
- It's something, isn't it?
- With Candice Bergen.

21
00:01:01,369 --> 00:01:03,803
- I know.
- I've always liked her.

22
00:01:03,972 --> 00:01:05,439
Remember her
in Carnal Knowledge?

23
00:01:05,607 --> 00:01:08,132
- Sure.
- She show her breasts in that?

24
00:01:08,710 --> 00:01:10,940
She's not really the naked type.

25
00:01:11,346 --> 00:01:13,109
I can't believe I missed Kramer.

26
00:01:13,514 --> 00:01:15,209
He asked me to go with him.

27
00:01:15,617 --> 00:01:17,881
- He did?
- Yeah. I turned him down.

28
00:01:18,052 --> 00:01:20,885
- How come you didn't tell me?
- He asked me to keep it a secret.

29
00:01:21,055 --> 00:01:22,784
- You can never keep a secret.
- I know.

30
00:01:22,957 --> 00:01:25,983
This was like a record. My previous
record was when Jodi Hirsch...

31
00:01:26,160 --> 00:01:28,458
...asked me not to tell anybody
we slept together.

32
00:01:28,630 --> 00:01:31,030
I kept a lid on that
for about 28 seconds.

33
00:01:31,499 --> 00:01:34,161
- Well, you've come a long way.
- I've matured.

34
00:01:34,335 --> 00:01:36,200
The Tonight Show
called and they want me...

35
00:01:36,371 --> 00:01:39,534
...to do the show on the 28th.
They're giving me free tickets to L.A.

36
00:01:39,707 --> 00:01:41,436
You wanna go?

37
00:01:41,609 --> 00:01:43,076
A free ticket?

38
00:01:43,478 --> 00:01:45,673
Yeah. In fact, we could
track down Kramer.

39
00:01:45,847 --> 00:01:48,680
I always felt bad about the way
he left. That was a mess.

40
00:01:48,850 --> 00:01:51,375
I never should've taken back
those keys.

41
00:01:51,586 --> 00:01:54,146
- What about accommodations?
- All taken care of.

42
00:01:54,522 --> 00:01:56,888
Is there a meal allowance?

43
00:01:57,058 --> 00:01:58,889
What about seat assignments?

44
00:01:59,060 --> 00:02:01,756
Could I have the kosher meal?
I hear the kosher meal is good.

45
00:02:01,929 --> 00:02:05,023
And I need clothes.
I gotta get a haircut.

46
00:02:05,366 --> 00:02:07,766
And I have to refill
my allergy medication.

47
00:02:08,503 --> 00:02:10,835
Do I need a hat?
I need a hat, don't I?

48
00:02:11,172 --> 00:02:14,266
Could we do the Universal tour?
They have that Backdraft exhibit.

49
00:02:14,442 --> 00:02:15,966
Now, that looks very cool to me.

50
00:02:16,144 --> 00:02:20,604
See, my acting technique...
My personal acting technique...

51
00:02:20,982 --> 00:02:24,145
...is working with color.
Imagining color...

52
00:02:24,352 --> 00:02:27,048
...and then finding the emotional,
vibrational mood...

53
00:02:27,221 --> 00:02:28,745
...connected to the color.

54
00:02:29,323 --> 00:02:31,951
If you look through my scripts
you'll see that my lines...

55
00:02:32,126 --> 00:02:35,653
...have a special color.
So I don't memorize the language.

56
00:02:35,830 --> 00:02:40,164
I memorize color. This way, I can
go through red, yellow, green, blue.

57
00:02:40,334 --> 00:02:43,497
And you have a full palette
of emotions.

58
00:02:43,671 --> 00:02:45,104
Hey.

59
00:02:45,373 --> 00:02:47,705
Didn't I tell you to get out of here?

60
00:02:47,975 --> 00:02:49,203
- Did you?
- Let's go.

61
00:02:49,377 --> 00:02:51,311
- I was just...
- Yeah, you were just nothing.

62
00:02:51,479 --> 00:02:53,140
Come on, let's go.

63
00:02:53,414 --> 00:02:55,006
We'll talk about this
a little later.

64
00:02:55,183 --> 00:02:57,549
- Let's go.
- Are you an actor?

65
00:02:58,853 --> 00:03:00,753
See you, Mike!

66
00:03:20,775 --> 00:03:23,801
- Murphy Brown.
- Yeah, Candice Bergen, please.

67
00:03:23,978 --> 00:03:25,775
Who's calling, please?

68
00:03:25,947 --> 00:03:28,313
Well, just tell her that it's Kramer.

69
00:03:29,817 --> 00:03:32,843
All right, I'll... I'll call her at home.

70
00:03:34,522 --> 00:03:36,990
Here. Go ahead. It's all yours.

71
00:03:37,759 --> 00:03:41,388
- Hello, Kramer.
- Oh, Helene. How are you?

72
00:03:41,863 --> 00:03:45,230
I haven't worked since 1934.
How do you think I am?

73
00:03:45,399 --> 00:03:47,458
Well, that's only...

74
00:03:47,969 --> 00:03:51,063
- Fifty-eight years.
- It was a Three Stooges short.

75
00:03:51,239 --> 00:03:52,467
"Sappy Pappies. "

76
00:03:52,640 --> 00:03:54,972
I played Mr. Sugarman's secretary,
remember?

77
00:03:55,143 --> 00:03:57,202
Right, yeah.
That was a Shemp, right?

78
00:03:57,378 --> 00:03:59,539
No, no. Curly.

79
00:03:59,714 --> 00:04:02,148
The boys played three sailors
who find a baby.

80
00:04:02,316 --> 00:04:04,978
The baby's been kidnapped,
and the police think they did it.

81
00:04:05,453 --> 00:04:07,318
- Right.
- Of course, they didn't do it.

82
00:04:07,488 --> 00:04:09,718
The police have made
an awful mistake.

83
00:04:09,891 --> 00:04:12,018
Moe hits Curly with an ax.

84
00:04:12,193 --> 00:04:14,821
- The Stooges catch the kidnappers.
- Right.

85
00:04:14,996 --> 00:04:16,930
- But it's too late.
- Really?

86
00:04:17,098 --> 00:04:19,794
- The baby's dead.
- Really?

87
00:04:19,967 --> 00:04:23,232
The boys are sent to death row
and are executed.

88
00:04:23,404 --> 00:04:25,304
Well, I don't remember that part.

89
00:04:25,473 --> 00:04:27,304
I play Mr. Sugarman's secretary.

90
00:04:27,475 --> 00:04:29,909
Oh, yeah, yeah. You were very good.

91
00:04:30,077 --> 00:04:33,240
It was sad for a Three Stooges,
what with the dead baby...

92
00:04:33,414 --> 00:04:35,905
...and the Stooges
being executed and all.

93
00:04:36,083 --> 00:04:39,177
Yeah, well, that was an unusual
choice for the Stooges.

94
00:04:39,654 --> 00:04:42,851
Would you like to buy me a fat-free
frozen yogurt at the store, Kramer?

95
00:04:43,024 --> 00:04:45,219
Well, you know, I can't right now,
you know.

96
00:04:45,393 --> 00:04:46,621
I got a very big meeting.

97
00:04:46,794 --> 00:04:49,092
I got these people interested
in my movie treatment.

98
00:04:49,263 --> 00:04:51,595
So we'll have to make it
another time, all right?

99
00:04:51,766 --> 00:04:54,462
No, no! No, no, don't go up there,
Kramer. They'll hurt you.

100
00:04:54,635 --> 00:04:57,570
You'll never make it in this town.
You're too sensitive, like me.

101
00:04:57,738 --> 00:05:00,605
Helene, you're wrong.
You know, I'm not that sensitive.

102
00:05:00,975 --> 00:05:04,741
I was engaged to Mickey Rooney.
He left me at the altar.

103
00:05:05,179 --> 00:05:06,806
Kramer.

104
00:05:06,981 --> 00:05:09,347
Kramer!

105
00:05:16,724 --> 00:05:18,851
- What is this?
- What?

106
00:05:19,026 --> 00:05:22,018
We're going on a two-day trip.
What are you, Diana Ross?

107
00:05:23,397 --> 00:05:26,855
I happen to dress based on mood.

108
00:05:28,102 --> 00:05:30,832
But you essentially wear
the same thing all the time.

109
00:05:31,005 --> 00:05:33,371
Seemingly. Seemingly.

110
00:05:33,641 --> 00:05:37,133
But within that basic framework,
there are many subtle variations...

111
00:05:37,311 --> 00:05:39,745
...only discernible
to an acute observer...

112
00:05:39,914 --> 00:05:42,439
...that reflect the many moods,
the many shades...

113
00:05:42,617 --> 00:05:44,915
...the many sides
of George Costanza.

114
00:05:45,086 --> 00:05:48,419
- What mood is this?
- This is "morning mist. "

115
00:05:53,227 --> 00:05:55,695
What do you figure? Twenty?

116
00:05:55,863 --> 00:05:58,388
- Twenty-one?
- Close enough.

117
00:05:58,566 --> 00:06:00,557
Forensics ought to be able
to nail it down.

118
00:06:00,835 --> 00:06:02,564
- No ID?
- No ID.

119
00:06:02,737 --> 00:06:06,264
- No witnesses?
- Just the trees, Johnny.

120
00:06:06,440 --> 00:06:08,499
Just the trees.

121
00:06:09,243 --> 00:06:10,938
Pretty young thing.

122
00:06:11,612 --> 00:06:13,102
She was.

123
00:06:13,281 --> 00:06:14,839
Not anymore.

124
00:06:15,349 --> 00:06:17,214
Somebody saw to that.

125
00:06:17,385 --> 00:06:19,353
Sure did, Johnny.

126
00:06:19,720 --> 00:06:21,881
Damn shame too.

127
00:06:22,290 --> 00:06:23,780
What do you make of it?

128
00:06:23,958 --> 00:06:25,550
I don't know.

129
00:06:27,028 --> 00:06:28,928
But I don't like it.

130
00:06:34,435 --> 00:06:36,699
Look at this guy.
He's like a cat burglar.

131
00:06:36,871 --> 00:06:40,773
He thinks if he goes through real slow,
the machine won't detect him.

132
00:06:40,975 --> 00:06:43,842
I've always been nervous
about going through these things.

133
00:06:44,011 --> 00:06:46,275
I'm afraid I'm gonna step
into another dimension.

134
00:06:46,447 --> 00:06:48,347
Just go.

135
00:06:51,152 --> 00:06:52,517
I made it.

136
00:06:52,687 --> 00:06:55,121
Empty your pockets, please.

137
00:06:55,523 --> 00:06:58,117
Walk through again, please.

138
00:06:58,359 --> 00:07:00,190
Are you sure you don't
have metal on you?

139
00:07:00,361 --> 00:07:03,819
- Bracelets, rings, anklets?
- Anklets? No.

140
00:07:03,998 --> 00:07:05,590
- A lot of men wear anklets.
- Really?

141
00:07:05,766 --> 00:07:08,257
- Yeah.
- What do you have in your bag, sir?

142
00:07:08,536 --> 00:07:10,026
- My bag?
- Step over here, please.

143
00:07:10,204 --> 00:07:12,900
- Over here?
- Do you have a knife in the bag?

144
00:07:13,207 --> 00:07:14,970
A knife?

145
00:07:15,309 --> 00:07:17,436
Open the bag, please.

146
00:07:20,948 --> 00:07:22,813
- What's this?
- Moisturizer.

147
00:07:23,317 --> 00:07:25,114
For your wife?

148
00:07:25,319 --> 00:07:27,344
No. I...

149
00:07:27,621 --> 00:07:29,350
I use it.

150
00:07:30,057 --> 00:07:32,457
Spread your arms and legs, please.

151
00:07:35,463 --> 00:07:39,422
Ladies and gentlemen,
I implore you.

152
00:07:40,668 --> 00:07:42,397
Have a good trip.

153
00:07:43,204 --> 00:07:44,432
- All right.
- That's it?

154
00:07:44,605 --> 00:07:45,833
- That's it.
- All right.

155
00:07:46,007 --> 00:07:47,531
Come on, Jerry, let's go.

156
00:07:48,576 --> 00:07:51,670
- What was that all about?
- I must have iron-rich blood.

157
00:07:51,846 --> 00:07:53,677
- Here we go. L.A.
- The coast.

158
00:07:53,848 --> 00:07:55,110
La-la land.

159
00:07:55,282 --> 00:07:57,273
- I got the window seat, right?
- Who said that?

160
00:07:57,451 --> 00:07:59,316
- I called it.
- Oh, no.

161
00:07:59,487 --> 00:08:03,082
It seems to me that the closest thing
we have to royalty in America...

162
00:08:03,257 --> 00:08:05,657
...are the people that get
to ride in those carts...

163
00:08:05,860 --> 00:08:07,293
...through the airport.

164
00:08:07,461 --> 00:08:09,895
Don't you hate these things?
They come out of nowhere.

165
00:08:10,064 --> 00:08:13,556
Beep-beep. "Cart people.
Look out. Cart people. Look out. "

166
00:08:13,734 --> 00:08:16,202
We all scurry out of the way
like worthless peasants.

167
00:08:17,271 --> 00:08:19,296
"It's cart people.

168
00:08:19,473 --> 00:08:21,600
I hope we didn't slow you down.

169
00:08:21,776 --> 00:08:26,076
Wave to the cart people, Timmy.
They're the best people in the world. "

170
00:08:26,514 --> 00:08:28,846
You know, if you're too fat,
slow and disoriented...

171
00:08:29,016 --> 00:08:32,952
...to get to your gate on time,
you're not ready for air travel.

172
00:08:33,120 --> 00:08:35,714
I hate the people that get
onto the moving walkway...

173
00:08:35,890 --> 00:08:37,983
...and then just stand there.

174
00:08:38,159 --> 00:08:40,184
Like it's a ride.

175
00:08:40,361 --> 00:08:44,229
"Excuse me, there's no animated
pirates or bears along the way here.

176
00:08:44,398 --> 00:08:47,697
Do your legs work at all?"

177
00:09:05,753 --> 00:09:07,243
Yeah, I'm here for the audition.

178
00:09:07,421 --> 00:09:10,015
Which audition? The music video,
the horror movie...

179
00:09:10,191 --> 00:09:11,988
...the exercise tape,
or the infomercial?

180
00:09:14,829 --> 00:09:17,992
Let's see. Well...

181
00:09:54,435 --> 00:09:56,460
You scream good.

182
00:09:57,171 --> 00:09:58,763
You too.

183
00:10:04,245 --> 00:10:07,806
- So can I keep this treatment?
- Oh, yeah, yeah. I got 20 copies.

184
00:10:07,982 --> 00:10:10,473
Because I can show it
to my manager.

185
00:10:10,651 --> 00:10:13,313
He has connections with
West German television money.

186
00:10:13,487 --> 00:10:16,684
Yeah, they're trying to put together
a miniseries for me on Eva Braun.

187
00:10:16,857 --> 00:10:18,848
I mean, think about it.
Is that a great idea?

188
00:10:19,026 --> 00:10:22,518
We know nothing about Eva Braun,
only that she was Hitler's girlfriend.

189
00:10:22,696 --> 00:10:24,994
What was it like having sex
with Adolf Hitler?

190
00:10:25,166 --> 00:10:26,656
What do you wear in a bunker?

191
00:10:26,834 --> 00:10:29,064
What did her parents think of Hitler
as a potential son-in-law?

192
00:10:29,236 --> 00:10:31,227
- I could go on and on...
- Wait, wait, hold on.

193
00:10:31,772 --> 00:10:34,070
Look who's over there.
Don't look, don't look.

194
00:10:34,241 --> 00:10:36,072
It's Fred Savage.

195
00:10:36,710 --> 00:10:39,304
- Big deal.
- He'd be perfect for my movie.

196
00:10:40,381 --> 00:10:42,576
This is a once-in-a-lifetime
opportunity.

197
00:10:44,718 --> 00:10:48,677
I gotta go over there. I gotta give him
a copy of my treatment.

198
00:10:48,856 --> 00:10:50,483
Why are you breathing so hard?

199
00:10:50,658 --> 00:10:52,751
I'm just a little nervous.

200
00:10:52,927 --> 00:10:55,327
Okay, I gotta relax.

201
00:10:56,564 --> 00:10:59,727
All right. Wish me luck, huh?

202
00:11:05,773 --> 00:11:07,604
Hey. Oh, did I frighten you?

203
00:11:07,775 --> 00:11:10,039
I'm not crazy. I look weird,
but I'm just like you.

204
00:11:10,211 --> 00:11:13,112
I'm just a regular guy trying
to make it in this business.

205
00:11:13,514 --> 00:11:15,539
You know, I really like your work.
The...

206
00:11:15,983 --> 00:11:17,678
- Thank you.
- I can't remember the name.

207
00:11:17,885 --> 00:11:20,080
- My mind's a blank. I'm nervous.
- That's okay.

208
00:11:20,254 --> 00:11:21,551
- You know...
- Just relax.

209
00:11:21,722 --> 00:11:23,952
Okay, yeah. But I got...

210
00:11:24,458 --> 00:11:26,790
Stupid table.
I'm not normally like this.

211
00:11:26,961 --> 00:11:29,521
Usually I'm cool and charming.
I don't mean to bother you.

212
00:11:29,697 --> 00:11:32,461
It's fate that you happened
to be here at the same time as me.

213
00:11:32,866 --> 00:11:34,595
It's fate. You can't avoid your fate.

214
00:11:34,768 --> 00:11:36,827
I got this treatment
I think you'd be great in.

215
00:11:37,004 --> 00:11:39,131
- I'll give it to you.
- Yeah, okay. Thank you.

216
00:11:39,306 --> 00:11:41,240
Excuse me.
Oh, wait. Are you going now?

217
00:11:41,408 --> 00:11:42,807
- Bye.
- Wait, wait.

218
00:11:47,615 --> 00:11:51,449
Yeah, that's Kramer. K-R-A-M-E-R.

219
00:11:52,620 --> 00:11:55,953
I don't know, wavy. George, how
would you describe Kramer's hair?

220
00:11:56,423 --> 00:11:58,152
- Curly.
- Wavy.

221
00:11:58,492 --> 00:12:00,960
- What did you ask me for?
- Yeah. I'll hold on.

222
00:12:01,128 --> 00:12:03,688
George, did you see a piece
of paper on the nightstand...

223
00:12:03,864 --> 00:12:06,458
...like, crumpled up like a napkin?
- No.

224
00:12:06,800 --> 00:12:09,564
Because I had three jokes on it.
They were perfectly worded...

225
00:12:09,737 --> 00:12:11,705
...just the way I wanted to have it.

226
00:12:11,872 --> 00:12:13,703
I can't find it.

227
00:12:13,874 --> 00:12:15,307
Hello?

228
00:12:15,476 --> 00:12:17,376
Hey, a shoe-buffing machine.

229
00:12:17,611 --> 00:12:20,603
I don't know, 6'3"?
George, how tall is Kramer?

230
00:12:20,781 --> 00:12:23,079
You got your own shampoo,
conditioner...

231
00:12:23,250 --> 00:12:26,811
Body lotion, Jerry, body lotion.

232
00:12:26,987 --> 00:12:30,115
- About 6'3".
- A shower cap.

233
00:12:31,325 --> 00:12:33,293
Come in.

234
00:12:36,430 --> 00:12:38,364
Hello. I have more towels.

235
00:12:38,666 --> 00:12:43,399
Oh, good, good. Come in, come in.
Welcome. I'm George.

236
00:12:43,604 --> 00:12:45,834
- Hello.
- Hi. And this is Jerry...

237
00:12:46,006 --> 00:12:48,600
...over there on the phone.
That's Jerry. And you are..?

238
00:12:48,776 --> 00:12:51,472
- Lupe.
- Lupe. That's very nice.

239
00:12:51,645 --> 00:12:54,079
Listen, are you gonna be
making the bed in the morning?

240
00:12:54,348 --> 00:12:55,940
- Oh, yes.
- Fine, excellent.

241
00:12:56,116 --> 00:12:58,175
Could you do me a favor?

242
00:12:58,352 --> 00:13:01,321
Could you not tuck the blankets in?

243
00:13:01,488 --> 00:13:03,388
Because I can't sleep all tucked in.

244
00:13:03,557 --> 00:13:05,218
- Oh, yes, yes.
- Yes.

245
00:13:05,392 --> 00:13:07,553
I like to just be able
to take the blankets...

246
00:13:07,728 --> 00:13:10,253
...and swish them and swirl them.
You know what I mean?

247
00:13:10,431 --> 00:13:14,162
You know, I don't like them
all tucked in. I just don't.

248
00:13:14,335 --> 00:13:18,169
I like to have a lot of room. I like
to have my toes pointed up in the air.

249
00:13:18,339 --> 00:13:20,739
I just like to scrunch up the blankets.

250
00:13:20,908 --> 00:13:23,638
Yes, yes. It's too tight to sleep.

251
00:13:23,811 --> 00:13:26,109
Exactly. You know
what I'm talking about, right?

252
00:13:26,313 --> 00:13:28,508
Yes. It's too tight.

253
00:13:29,450 --> 00:13:32,385
- Him too?
- You want your blankets tucked in?

254
00:13:32,786 --> 00:13:34,447
- What?
- Want your blankets tucked in?

255
00:13:34,621 --> 00:13:36,714
- What blankets?
- When Lupe makes up the beds.

256
00:13:36,890 --> 00:13:38,915
I don't know. Whatever they do.

257
00:13:39,093 --> 00:13:40,788
I tuck in, yes?

258
00:13:40,961 --> 00:13:42,861
Tuck in. Tuck in.

259
00:13:43,030 --> 00:13:46,625
All right. So that's one tuck
and one no-tuck.

260
00:13:46,834 --> 00:13:48,768
- Okay.
- Yes. One second, sweetheart.

261
00:13:49,236 --> 00:13:51,204
Jerry, it'd be easier if you didn't tuck.

262
00:13:51,638 --> 00:13:54,129
Excuse me. Fine.
You don't want me to tuck...

263
00:13:54,308 --> 00:13:56,333
...put me down for a no-tuck.

264
00:13:56,543 --> 00:13:58,408
It's two no-tucks.

265
00:13:58,879 --> 00:14:02,440
Hang on a second. You know what?
Changed my mind. Make it a tuck.

266
00:14:02,950 --> 00:14:06,408
- You just said you weren't tucking.
- I'm tucking.

267
00:14:06,720 --> 00:14:10,656
Hello? Hello? They hung up on me.

268
00:14:10,858 --> 00:14:12,553
They don't know
where Kramer is anyway.

269
00:14:12,726 --> 00:14:17,493
Alrighty. So that's one tuck
and one no-tuck. You got that?

270
00:14:19,099 --> 00:14:22,364
Excuse me. Did you see a piece
of paper on the nightstand here...

271
00:14:22,536 --> 00:14:24,470
...earlier today,
crumpled up like a napkin?

272
00:14:24,638 --> 00:14:27,937
Oh, yes, yes. I throw away
when we clean the room.

273
00:14:28,108 --> 00:14:30,042
- Oh, okay. Thanks.
- Thank you.

274
00:14:30,210 --> 00:14:32,770
Thank you. All right, Lupe,
bye-bye now.

275
00:14:32,946 --> 00:14:34,436
- Bye.
- Bye-bye.

276
00:14:34,615 --> 00:14:36,947
I can't believe she threw that out.

277
00:14:37,451 --> 00:14:41,182
I had the perfect wording of a joke
I was gonna do about the airport.

278
00:14:41,355 --> 00:14:43,016
I was gonna do it
on The Tonight Show.

279
00:14:43,190 --> 00:14:44,248
Now I can't remember it.

280
00:14:44,425 --> 00:14:46,484
What did you want?
You left it on the table.

281
00:14:46,660 --> 00:14:48,753
They're not supposed
to throw everything out.

282
00:14:48,929 --> 00:14:52,456
Hey, hey!
It's not Lupe's fault.

283
00:14:52,633 --> 00:14:54,567
- You shouldn't have left it out.
- All right.

284
00:14:54,768 --> 00:14:57,236
Just get your thing together
and let's get out of here.

285
00:14:57,404 --> 00:15:01,363
All right. Now, what mood am I in?
What mood am I..?

286
00:15:01,542 --> 00:15:03,669
- You shouldn't have tucked.
- I like it tucked.

287
00:15:03,844 --> 00:15:06,369
Nobody tucks anymore.

288
00:15:25,532 --> 00:15:26,999
- Hey, lieutenant?
- Yeah.

289
00:15:27,167 --> 00:15:29,465
This was found on her person.

290
00:15:29,636 --> 00:15:31,501
On her person?

291
00:15:32,039 --> 00:15:35,975
- What kind of expression is that?
- I don't know, sir. Police lingo.

292
00:15:36,143 --> 00:15:37,371
Oh, yeah?

293
00:15:38,312 --> 00:15:39,836
What's your name, son?

294
00:15:40,347 --> 00:15:42,713
- Ross.
- Ross.

295
00:15:42,883 --> 00:15:45,716
- You see that person there, Ross?
- Yes, sir.

296
00:15:45,919 --> 00:15:47,318
She's dead.

297
00:15:47,488 --> 00:15:49,149
- Have you got that?
- Yes, sir.

298
00:15:49,323 --> 00:15:50,551
Good.

299
00:15:50,891 --> 00:15:53,325
Get out before you find yourself
on transit patrol...

300
00:15:53,494 --> 00:15:56,088
...writing tickets to senior citizens
with fake bus passes.

301
00:15:56,864 --> 00:15:58,525
Yes, sir.

302
00:16:04,238 --> 00:16:06,900
I think we just caught a break.

303
00:16:10,110 --> 00:16:12,010
This is very exciting.

304
00:16:12,212 --> 00:16:15,272
You're on The Tonight Show. NBC.
Who else is on the show?

305
00:16:15,449 --> 00:16:17,679
- I don't know.
- You might meet a celebrity.

306
00:16:17,851 --> 00:16:19,751
I can't believe she threw out
my napkin.

307
00:16:19,920 --> 00:16:22,980
What are you worried about?
You know it.

308
00:16:23,190 --> 00:16:26,717
- You're gonna be all right here?
- Yeah. Go about your business.

309
00:16:26,894 --> 00:16:29,920
- I'll just wander around.
- All right. Don't wander too far.

310
00:16:30,097 --> 00:16:31,758
I'll meet you back here
in 15 minutes.

311
00:16:31,932 --> 00:16:34,924
Go, go. Don't worry about me.

312
00:16:47,014 --> 00:16:48,345
Hey.

313
00:16:49,283 --> 00:16:51,148
Corbin Bernsen.

314
00:16:51,685 --> 00:16:53,346
How you doing?

315
00:16:55,122 --> 00:16:59,650
- Big fan. Big fan.
- Thank you.

316
00:17:02,162 --> 00:17:04,790
- Hey, you grew a beard, huh?
- Yeah.

317
00:17:04,965 --> 00:17:08,492
Yeah. Yeah, I'm doing a movie
during my hiatus.

318
00:17:08,835 --> 00:17:10,700
Hey, you know...

319
00:17:10,904 --> 00:17:14,465
Do I have a case for you guys
to do on L.A. Law.

320
00:17:15,375 --> 00:17:16,774
Really.

321
00:17:18,445 --> 00:17:22,279
So mind you, at this point, I'm only
going out with her two, three weeks.

322
00:17:22,516 --> 00:17:26,748
So she goes out of town,
and she asks me to feed her cat.

323
00:17:27,754 --> 00:17:33,886
So, at this time, there's a lot of stuff
going on in my life...

324
00:17:34,061 --> 00:17:38,054
...and it slips my mind
for a few days.

325
00:17:38,231 --> 00:17:40,791
Maybe a week. Not even a week.
Five, six days.

326
00:17:40,968 --> 00:17:42,936
Yeah, yeah, yeah.
So, what happened?

327
00:17:43,103 --> 00:17:47,062
Well, it's the damnedest thing.

328
00:17:47,274 --> 00:17:49,538
The cat dies.

329
00:17:50,944 --> 00:17:55,040
So she comes back. She finds the cat,
lying on the carpet, stiff as a board.

330
00:17:55,215 --> 00:17:58,013
- So you killed the cat?
- That's what she says.

331
00:17:58,185 --> 00:18:00,619
I say, "Listen...

332
00:18:00,787 --> 00:18:04,655
It was an old cat.
It died of natural causes. "

333
00:18:04,825 --> 00:18:09,285
So get this. Now she tells me
that I gotta buy her a brand-new cat.

334
00:18:09,830 --> 00:18:12,765
I say, "Listen, honey,
first of all...

335
00:18:13,000 --> 00:18:16,697
...it was a pretty old cat. I'm not
gonna buy you a brand-new cat...

336
00:18:16,870 --> 00:18:18,735
...to replace an old dying cat.

337
00:18:18,905 --> 00:18:21,169
And second of all,
I go out to the garbage...

338
00:18:21,341 --> 00:18:23,741
...I find you a new cat
in 15 seconds. "

339
00:18:23,910 --> 00:18:27,869
I say, "You show me an autopsy report
that says this cat died of starvation...

340
00:18:28,048 --> 00:18:30,312
...I spring for a new cat. "

341
00:18:30,517 --> 00:18:33,509
So she says something to me,
like, I don't know...

342
00:18:33,687 --> 00:18:36,918
"Get the hell out of here. "
And she breaks up with me.

343
00:18:38,058 --> 00:18:41,494
Now, don't you think that would be
a great case on L.A. Law?

344
00:18:45,432 --> 00:18:47,423
I don't wanna tell you
how to run your show...

345
00:18:47,601 --> 00:18:49,068
No, of course not.

346
00:18:49,236 --> 00:18:51,830
It's enough with the bar already,
you know what I'm saying?

347
00:18:52,272 --> 00:18:54,570
Have they thought
about changing the setting?

348
00:18:54,741 --> 00:18:55,935
Doubt it. I doubt it. Yeah.

349
00:18:56,109 --> 00:18:58,236
Because people do meet in places
besides a bar.

350
00:18:58,412 --> 00:18:59,902
Well, yeah, they do.

351
00:19:00,113 --> 00:19:02,604
What about a rec room, huh?

352
00:19:02,783 --> 00:19:04,250
Or a community center.

353
00:19:04,418 --> 00:19:05,885
You ought to write one of those.

354
00:19:06,053 --> 00:19:08,385
I'll bring it up with the producers.
I gotta...

355
00:19:08,555 --> 00:19:11,786
Fabulous. I'll think about that,
George. Thank you.

356
00:19:12,592 --> 00:19:14,355
- How's it going?
- Great, great.

357
00:19:14,528 --> 00:19:18,089
I actually just had two meaningful,
intelligent conversations...

358
00:19:18,265 --> 00:19:20,130
...with Corbin Bernsen
and George Wendt.

359
00:19:20,300 --> 00:19:23,133
- Really?
- Yeah. Not fan talk. Not gushing.

360
00:19:23,303 --> 00:19:26,602
You know? Actual conversation.
I was incredibly articulate.

361
00:19:27,708 --> 00:19:30,438
You got toilet paper
on your heel there.

362
00:19:35,415 --> 00:19:37,849
It's The Tonight Show with Jay Leno.

363
00:19:38,018 --> 00:19:41,078
Tonight Jay welcomes
Corbin Bernsen, George Wendt...

364
00:19:41,254 --> 00:19:43,882
...and comedian, Jerry Seinfeld.

365
00:19:45,759 --> 00:19:49,354
Oh, yeah. People are always trying
to give me a great case for L.A. Law.

366
00:19:49,529 --> 00:19:51,963
Just a few seconds ago,
right outside in the hallway...

367
00:19:52,132 --> 00:19:54,930
...this nut, some sick nut
comes up to me...

368
00:19:55,102 --> 00:19:58,765
...and says he was supposed
to watch this girl's cat...

369
00:19:58,939 --> 00:20:02,238
...while she was away out of town.
Anyway, he forgets to feed the cat.

370
00:20:02,409 --> 00:20:05,207
The cat dies, starves to death.
He kills the cat.

371
00:20:05,378 --> 00:20:07,346
Refuses to get her a new one.

372
00:20:07,514 --> 00:20:10,005
Won't give her any money,
won't pay her.

373
00:20:10,183 --> 00:20:12,674
And he wants Arnie Becker
to represent him. Nice guy.

374
00:20:12,853 --> 00:20:14,844
Yeah, that'd make a great case
for L.A. Law.

375
00:20:15,255 --> 00:20:16,984
Thanks a lot.

376
00:20:18,625 --> 00:20:22,789
He's a very handsome man.
Passionate, intense.

377
00:20:23,630 --> 00:20:26,258
But troubled, strange.

378
00:20:26,433 --> 00:20:29,061
I think he may be in love with me.

379
00:20:29,236 --> 00:20:33,297
Of course, there's nothing abnormal
about that. I have many suitors.

380
00:20:33,607 --> 00:20:35,734
It's funny, because
even after all these years...

381
00:20:35,909 --> 00:20:40,141
...we still get people giving us advice
how to improve the show.

382
00:20:40,313 --> 00:20:43,441
Actually, a few moments ago,
I ran into a nut back there...

383
00:20:43,617 --> 00:20:45,983
...who said that maybe
we should think about...

384
00:20:46,153 --> 00:20:48,815
...you know, not doing the show
in a bar.

385
00:20:53,560 --> 00:20:56,961
So that's when I said,
"Hey, Kramer, dude...

386
00:20:57,130 --> 00:20:59,530
...you ever kill a man before?"

387
00:20:59,833 --> 00:21:02,768
And he said,
"What do you think, junior?

388
00:21:02,969 --> 00:21:07,030
These hands have been
soaking in Ivory Liquid?"

389
00:21:07,207 --> 00:21:09,198
That guy you talked to,
what did he look like?

390
00:21:09,376 --> 00:21:13,312
- Short little bald guy with glasses.
- That's the same guy I talked to.

391
00:21:13,480 --> 00:21:15,539
Never ends, does it?

392
00:21:16,116 --> 00:21:21,179
So I'm going through the airport,
and I put my bag on that little...

393
00:21:21,354 --> 00:21:24,152
The... The conveyor belt.

394
00:21:30,230 --> 00:21:32,528
Issue an arrest warrant.
Put out an APB.

395
00:21:32,699 --> 00:21:34,826
Let's pick up this...

396
00:21:35,101 --> 00:21:36,659
...Kramer.

397
00:21:38,271 --> 00:21:41,570
- I was terrible.
- What, are you crazy? You were fine.

398
00:21:41,741 --> 00:21:43,902
I couldn't remember
what I was trying to say.

399
00:21:44,077 --> 00:21:46,136
- That whole thing about the...
- Conveyor belt.

400
00:21:46,313 --> 00:21:49,248
Yeah. Because she threw out
my napkin.

401
00:21:49,916 --> 00:21:52,214
I can't believe...
You're blaming Lupe?

402
00:21:52,385 --> 00:21:55,877
Yes. Lupe. I'm blaming Lupe.

403
00:21:57,123 --> 00:21:59,114
Our top story tonight:
There's been a break...

404
00:21:59,292 --> 00:22:03,126
...in the so-called Smog Stranglings.
Police have just released a photo...

405
00:22:03,296 --> 00:22:06,322
...of the suspect being sought
in connection with the slaying.

406
00:22:06,499 --> 00:22:10,595
He is known only as Kramer.

407
00:22:15,275 --> 00:22:17,505
Talk show hosts
never seem to have any idea...

408
00:22:17,677 --> 00:22:19,702
...how much time is left in the show.

409
00:22:19,880 --> 00:22:22,178
They're always looking off-camera:
"Do we have time?

410
00:22:22,349 --> 00:22:24,010
How are we doing on time?

411
00:22:24,184 --> 00:22:26,948
Anybody know what the time is?
What's the time?"

412
00:22:27,220 --> 00:22:29,313
You never see Magnum P.I. go,
"Should I strangle this guy...

413
00:22:29,489 --> 00:22:31,514
...or are we gonna take a break here?

414
00:22:31,691 --> 00:22:34,751
Can you stay for another beating?
I'll bop him in the head.

415
00:22:34,928 --> 00:22:38,921
We'll do a commercial, come back,
I'll drive the car real fast. Stay with us. "


from heapq import merge
import string

allowed = "0123456789:->, \n"
firstSubtitleFileName = "Seinfeld [04x01] - The Trip (1).srt"
secondSubtitleFileName = "Seinfeld [04x01] - The Trip (1).srt"

firstSubtitleFile = open(firstSubtitleFileName, "r")
secondSubtitleFile = open(secondSubtitleFileName, "r")

firstSubtitleList = []
secondSubtitleList= []

for line in firstSubtitleFile:
    strippedLine = line.strip()
    firstSubtitleList.append(strippedLine+"\n")
    secondSubtitleList.append(strippedLine+"\n")
    
firstSubtitleFile.close()
secondSubtitleFile.close()

mergeFile = open('merge' + firstSubtitleFileName, "w")

index = -1
for line in firstSubtitleList:
    index = index + 1
    mergeFile.write(line)
    if not all(ch in allowed for ch in line):
        mergeFile.write(secondSubtitleList[index])
    
mergeFile.close()
